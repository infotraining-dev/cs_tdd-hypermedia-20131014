﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WatiN.Core;

namespace BDD.Calculator.Acceptance_Tests_UI.Steps
{
    public static class WebBrowser
    {
        public static IE Current
        {
            get
            {
                if (!ScenarioContext.Current.ContainsKey("browser"))
                {
                    ScenarioContext.Current["browser"] = new IE();
                }

                return ScenarioContext.Current["browser"] as IE;
            }
        }
    }

    [Binding]
    class AddNumbersUISteps
    {
        [AfterScenario]
        public void CleanUp()
        {
            WebBrowser.Current.Close();
            WebBrowser.Current.Dispose();
        }

        [When(@"user navigates to Math page")]
        public void WhenUserNavigatesToMathPage()
        {
            WebBrowser.Current.GoTo("http://localhost:14493/Math/AddNumbers");
        }

        [Then(@"a Math page should be displayed")]
        public void ThenAMathPageShouldBeDisplayed()
        {
            WebBrowser.Current.ContainsText("Calculator");
        }

        [Then(@"page title should be ""(.*)""")]
        public void ThenPageTitleShouldBe(string title)
        {
            Assert.That(WebBrowser.Current.Title, Is.EqualTo(title));
        }

        [Given(@"I have navigated to Math Page")]
        public void GivenIHaveNavigatedToMathPage()
        {
            WebBrowser.Current.GoTo("http://localhost:14493/Math/AddNumbers");
        }

        [Given(@"I have entered (.*) into the calculator as a first number")]
        public void GivenIHaveEnteredIntoTheCalculatorAsAFirstNumber(int firstNumber)
        {
            WebBrowser.Current.TextField("FirstNumber").TypeText(firstNumber.ToString());
        }

        [Given(@"I have entered (.*) into the calculator as a second number")]
        public void GivenIHaveEnteredIntoTheCalculatorAsASecondNumber(int secondNumber)
        {
            WebBrowser.Current.TextField("SecondNumber").TypeText(secondNumber.ToString());
        }

        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            WebBrowser.Current.Button("Add").Click();
        }

        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int result)
        {
            Assert.IsTrue(WebBrowser.Current.ContainsText("Result: " + result));
        }

    }
}
