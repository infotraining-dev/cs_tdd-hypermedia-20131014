﻿Feature: AddNumbersUI
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

Scenario: Navigation to Math Page
	When user navigates to Math page
	Then a Math page should be displayed
	And page title should be "Math Calculator" 

@mytag
Scenario: Add two numbers
	Given I have navigated to Math Page
	And I have entered 50 into the calculator as a first number
	And I have entered 70 into the calculator as a second number
	When I press add
	Then the result should be 120 on the screen