﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BDD.Calculator.Domain;
using BDD.Calculator.UI.Web.Controllers;
using BDD.Calculator.UI.Web.Models;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace BDD.Calculator.Acceptance_Tests.Steps
{
    [Binding]
    class NavigationToMathPageSteps
    {
        private MathController _controller;
        private ActionResult _result;

        [BeforeScenario]
        public void Init()
        {
            _controller = new MathController(new MathService());
        }

        [When(@"user navigates to Math page")]
        public void WhenUserNavigatesToMathPage()
        {
            _result = _controller.AddNumbers();
        }

        [Then(@"a Math page should be displayed")]
        public void ThenAMathPageShouldBeDisplayed()
        {
            Assert.IsInstanceOf<ViewResult>(_result);
        }

        [Then(@"page title should be ""(.*)""")]
        public void ThenPageTitleShouldBe(string p0)
        {
            Assert.AreEqual("Math Calculator", _controller.ViewBag.Title);
        }
    }

    [Binding]
    class AddNumbersSteps
    {
        private MathController _controller;
        private ViewResult _result;
        private AddNumbersModel _model;

        [BeforeScenario]
        public void Init()
        {
            _model = new AddNumbersModel();
            _controller = new MathController(new MathService());
        }

        [Given(@"I have entered (.*) into the calculator as a first number")]
        public void GivenIHaveEnteredIntoTheCalculatorAsAFirstNumber(int firstNumber)
        {
            _model.FirstNumber = firstNumber;
        }

        [Given(@"I have entered (.*) into the calculator as a second number")]
        public void GivenIHaveEnteredIntoTheCalculatorAsASecondNumber(int secondNumber)
        {
            _model.SecondNumber = secondNumber;
        }


        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            _result = _controller.AddNumbers(_model);
        }

        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int expectedResult)
        {
            int actualResult = (_result.Model as AddNumbersModel).Result.Value;

            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }
    }
}
