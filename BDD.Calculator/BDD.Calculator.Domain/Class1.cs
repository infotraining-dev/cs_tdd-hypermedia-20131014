﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDD.Calculator.Domain
{
    public interface IMathService
    {
        int Add(int first, int second);
    }

    public class MathService : IMathService
    {
        public int Add(int first, int second)
        {
            return first + second;
        }
    }
}
