﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BDD.Calculator.Domain;
using BDD.Calculator.UI.Web.Models;

namespace BDD.Calculator.UI.Web.Controllers
{
    public class MathController : Controller
    {
        private readonly IMathService _mathService;

        public MathController(IMathService mathService)
        {
            _mathService = mathService;
        }

        //
        // GET: /Math/

        public ActionResult AddNumbers()
        {
            ViewBag.Title = "Math Calculator";

            return View();
        }

        [HttpPost]
        public ViewResult AddNumbers(AddNumbersModel model)
        {
            model.Result = _mathService.Add(model.FirstNumber, model.SecondNumber);
            
            return View(model);
        }
    }
}
