﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BDD.Calculator.Domain;
using BDD.Calculator.UI.Web.Controllers;
using BDD.Calculator.UI.Web.Models;
using Machine.Specifications;
using Machine.Specifications.Mvc;
using Moq;
using It = Machine.Specifications.It;
using Arg = Moq.It;

namespace BDD.Calculator.Web.Specs
{
    /*

    [TestFixture]
    class CalculatorTests
    {
        [Test]
        public void when_addding_two_numbers_it_should_return_correct_value()
        {
            // Arrange
            Calculator calc = new Calculator();

            // Act
            int result = calc.Add(1, 2);

            // Assert
            Assert.That(result, Is.Equal(3));
        }
    }

    [Subject(typeof (Calculator))]
    class when_addding_two_numbers
    {
        private static Calculator calc;
        private static int result;

        Establish context = () => calc = new Calculator();

        private Because of = () => result = calc.Add(1, 2);

        private It should_return_correct_value = () => result.ShouldEqual(3);
    }

    */


    [Subject(typeof (MathController))]
    class when_adding_two_numbers
    {
        private Establish context = () =>
        {
            _firstNumber = 10;
            _secondNumber = 20;
            _service = new Mock<IMathService>();
            _controller = new MathController(_service.Object);
            _model = new AddNumbersModel() {FirstNumber = _firstNumber, SecondNumber = _secondNumber};
            
            _service
                .Setup(m => m.Add(Arg.IsAny<int>(), Arg.IsAny<int>()))
                .Returns(_firstNumber + _secondNumber);
        };

        private Because of = () => _result = _controller.AddNumbers(_model);

        private It should_return_a_view = () => _result.ShouldBeAView();

        private It should_return_model = () =>
        {
            _result.ShouldHaveModelOfType<AddNumbersModel>();
        };

        private It should_call_add_on_service = () =>
        {
            _service.Verify(m => m.Add(_firstNumber, _secondNumber));
        };

        private It should_return_result_of_addition = () =>
        {
            _result.Model<AddNumbersModel>().Result
                .ShouldEqual(_firstNumber + _secondNumber);
        };

        private static MathController _controller;
        private static AddNumbersModel _model;
        private static ViewResult _result;
        private static Mock<IMathService> _service;
        private static int _firstNumber;
        private static int _secondNumber;
    }
}
