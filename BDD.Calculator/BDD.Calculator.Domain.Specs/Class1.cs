﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;

namespace BDD.Calculator.Domain.Specs
{
    [Subject(typeof(MathService))]
    public class when_calculating_a_sum_of_two_numbers
    {
        private Establish context = () => _service = new MathService();

        private Because of = () => _result = _service.Add(10, 20);

        public It should_return_correct_result = () => _result.ShouldEqual(30);

        private static MathService _service;
        private static int _result;
    }
}
