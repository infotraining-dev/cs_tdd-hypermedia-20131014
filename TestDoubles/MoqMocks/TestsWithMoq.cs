﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace MoqMocks
{
    [TestClass]
    public class TestsWithMoq
    {
        private ILongRunningLibrary _longRunningLibrary;

        [TestInitialize]
        public void SetupTest()
        {
            var moqLongRunningLibrary = new Mock<ILongRunningLibrary>();

            moqLongRunningLibrary
                .Setup(lrl => lrl.RunForALongTime(It.IsAny<int>()))
                .Returns( (int s) => string.Format("This method has been mocked. The input value was {0}.", s));

            moqLongRunningLibrary
                .Setup(lrl => lrl.RunForALongTime(0))
                .Throws(new ArgumentException("0 is not a valid parameter."));

            _longRunningLibrary = moqLongRunningLibrary.Object;
        }

        [TestMethod]
        public void TestLongRunningLibrary()
        {
            const int interval = 120;
            var result = _longRunningLibrary.RunForALongTime(interval);
            Debug.WriteLine(string.Format("Result from method was: {0}", result));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ShouldThrowWhenArgumentEquals0()
        {
            const int interval = 0;
            _longRunningLibrary.RunForALongTime(interval);
        }
    }
}
