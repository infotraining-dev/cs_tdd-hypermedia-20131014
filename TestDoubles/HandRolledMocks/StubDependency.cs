﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HandRolledMocks
{
    class StubDependency : IDependency
    {
        public int GetValue(string s)
        {
            if (s == "abc")
                return 1;

            if (s == "xyz")
                return 2;

            return 0;
        }


        public void CallMeFirst()
        {
            throw new NotImplementedException();
        }

        public int CallMeTwice(string s)
        {
            throw new NotImplementedException();
        }

        public void CallMeLast()
        {
            throw new NotImplementedException();
        }
    }
}
