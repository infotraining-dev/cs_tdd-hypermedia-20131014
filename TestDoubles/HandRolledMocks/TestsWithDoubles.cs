﻿using System;
using NUnit.Framework;

namespace HandRolledMocks
{
    [TestFixture]
    public class TestsWithDoubles
    {
        [Test]
        public void TestWithDummy()
        {
            var dependency = new DummyDependency();
            var dependentClass = new DependentClass(dependency);
            const string param = "abc";
            const int expectedResult = 1;

            var result = dependentClass.GetValue(param);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void TestWithStub()
        {
            var dependency = new StubDependency();
            var dependentClass = new DependentClass(dependency);
            const string param1 = "abc";
            const string param2 = "xyz";
            const int expected1 = 1;
            const int expected2 = 2;

            var resultOne = dependentClass.GetValue(param1);
            var resultTwo = dependentClass.GetValue(param2);

            Assert.AreEqual(expected1, resultOne);
            Assert.AreEqual(expected2, resultTwo);
        }

        [Test]
        public void TestWithMock()
        {
            var dependency = new MockDependency();
            var dependentClass = new DependentClass(dependency);

            const string param1 = "abc";
            const string param2 = "xyz";
            const int expected1 = 1;
            const int expected2 = 2;

            dependentClass.CallMeFirst();
            var result1 = dependentClass.CallMeTwice(param1);
            var result2 = dependentClass.CallMeTwice(param2);
            dependentClass.CallMeLast();

            Assert.AreEqual(expected1, result1);
            Assert.AreEqual(expected2, result2);
        }
    }
}
