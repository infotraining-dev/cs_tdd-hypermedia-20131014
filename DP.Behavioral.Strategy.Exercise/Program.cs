﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DP.Behavioral.Strategy.Exercise
{
    public struct Result
    {
        public string Description;
        public double Value;

        public Result(string desc, double val)
        {
            Description = desc;
            Value = val;
        }
    }

    public enum StatType
    {
        Avg, Sum, Mediana
    }

    public interface IStatistics
    {
        void Calculate(double[] data, IList<Result> results);
    }

    public class Avg : IStatistics
    {
        public void Calculate(double[] data, IList<Result> results)
        {
            double avg = data.Average();
            results.Add(new Result("AVG", avg));
        }
    }

    public class Sum : IStatistics
    {
        public void Calculate(double[] data, IList<Result> results)
        {
            double sum = data.Sum();

            results.Add(new Result("SUM", sum));
        }
    }

    public class Mediana : IStatistics
    {
        public void Calculate(double[] data, IList<Result> results)
        {
            double mediana;

            double[] values = (double[])data.Clone();
            values.OrderBy(v => v);

            int length = values.Length;

            if (length % 2 != 0)
                mediana = values[(length + 1) / 2];
            else
                mediana = (values[length / 2] + values[length / 2 + 1]) / 2;

            results.Add(new Result("MEDIANA", mediana));
        }
    }


    public class DataAnalyzer
    {
        double[] Data { get; set; }
        List<Result> results = new List<Result>();

        public DataAnalyzer()
        {
        }

        public virtual void LoadData(string path)
        {
            List<double> values = new List<double>();

            using (StreamReader reader = File.OpenText(path))
            {
                string line = reader.ReadToEnd();

                foreach (string token in line.Split(' '))
                    values.Add(double.Parse(token));
            }

            Data = values.ToArray();

            results.Clear();
        }

        public void CalcStats(IStatistics stat)
        {
            stat.Calculate(Data, results);
        }

        public void ShowStatistics()
        {
            Console.Write("Data: [ ");

            foreach (double d in Data)
                Console.Write("{0} ", d);
            Console.WriteLine("]");

            Console.WriteLine("\nStatistics for data:");
            foreach (var result in results)
                Console.WriteLine(" + {0} = {1}", result.Description, result.Value);
        }   
    }

    class Program
    {
        public static void Main()
        {
            DataAnalyzer dataAnalizer = new DataAnalyzer();
            dataAnalizer.LoadData("test.dat");
            dataAnalizer.CalcStats(new Mediana());
            dataAnalizer.CalcStats(new Avg());
            dataAnalizer.CalcStats(new Sum());

            dataAnalizer.ShowStatistics();
        }
    }
}
