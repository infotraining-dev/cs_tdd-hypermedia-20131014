﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.Strategy.Exercise
{

    interface ISearch<in Criteria>
    {
        Item Find(Criteria criteria);
    }

    interface IRepository : ISearch<int>
    {
        void Add(Item item);
        void Delete(Item item);
        void Update(Item item);
        Item Find(int id);
    }

    class SearchController
    {
        private ISearch<int> _repo;

        public SearchController(ISearch<int> repo)
        {
            _repo = repo;
        }

        public void Index(int id)
        {
            Item item = _repo.Find(id);
        }
    }
}
