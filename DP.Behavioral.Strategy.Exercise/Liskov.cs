﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.Strategy.Exercise
{
    public abstract class Repository
    {
        public abstract void Add(Item item);
        public abstract void Delete(Item item);
    }

    public class DbRepository : Repository
    {
        public override void Add(Item item)
        {
            // Insert
        }

        public override void Delete(Item item)
        {
            // Delete
        }
    }

    public class ActiveDirRepository : Repository
    {
        public override void Add(Item item)
        {
            // dodanie do AD
        }

        public override void Delete(Item item)
        {
            throw new NotImplementedException();
        }
    }

    public class Item
    {
    }

    public class ItemService
    {
        public void CreateItem(Repository repo)
        {
            repo.Add(new Item());
        }

        public void RemoveItem(Repository repo)
        {
            if (repo.GetType() != typeof (ActiveDirRepository))
            {
                repo.Delete(new Item());
            }
            else
            {
                // Logger.Log("Cannot remove from AD");
            }
        }
    }

}
