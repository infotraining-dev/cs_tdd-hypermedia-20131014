﻿using Bowling;
using NUnit.Framework;

namespace Bowling_Tests
{
    [TestFixture]
    public class BowlingGameTests
    {
        private BowlingGame game;

        [SetUp]
        public void CreateGame()
        {
            game = new BowlingGame();
        }

        [Test, Category("Important")]
        public void WhenGameStartedScoreIsZero()
        {
            Assert.AreEqual(0, game.Score);
        }

        [Test]
        public void WhenAllThrowsInGutterScoreIsZero()
        {
            RollMany(20, 0);

            Assert.That(game.Score, Is.EqualTo(0));
        }

        [Test, Category("Important")]
        [TestCase(1, 20)]
        [TestCase(3, 60)]
        public void WhenAllThrowsNoMarksScoreIsSumOfPins(int pins, int score)
        {
            // Act
            RollMany(20, pins);

            // Assert
            Assert.That(game.Score, Is.EqualTo(score));
        }

        [Test]
        public void WhenSpareNextThrowIsCountedTwice()
        {
            RollSpare();
            game.Roll(3);
            RollMany(17, 0);

            Assert.That(game.Score, Is.EqualTo(16));
        }

        [Test]
        public void WhenStrikeNextTwoThrowsAreCountedTwice()
        {
            RollStrike();
            game.Roll(3);
            game.Roll(4);
            RollMany(16,0);

            Assert.That(game.Score, Is.EqualTo(24));
        }

        [Test]
        public void WhenPerfectGameThenMaximumScoreIs300()
        {
            for(int i = 0; i < 12; ++i)
                RollStrike();

            Assert.That(game.Score, Is.EqualTo(300));
        }

        private void RollStrike()
        {
            game.Roll(10);
        }

        private void RollSpare()
        {
            game.Roll(5);
            game.Roll(5);
        }

        private void RollMany(int rolls, int pins)
        {
            for (int i = 0; i < rolls; ++i)
                game.Roll(pins);
        }
    }
}