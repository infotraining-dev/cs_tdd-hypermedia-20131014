﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling
{
    public class BowlingGame
    {
        private int _currentRoll;
        private int[] _rolls = new int[21];

        public int Score
        {
            get
            {
                int score = 0;
                int roll = 0;

                for (int frame = 0; frame < 10; ++frame)
                {
                    if (IsStrike(roll)) // strike
                    {
                        score += 10 + StrikeBonus(roll);
                        roll++;
                    }
                    else if (IsSpare(roll))
                    {
                        score += 10 + SpareBonus(roll);
                        roll += 2;
                    }
                    else
                    {
                        score += _rolls[roll] + _rolls[roll + 1];
                        roll += 2;
                    }
                }

                return score;
            }
        }

        private int StrikeBonus(int roll)
        {
            return _rolls[roll + 1] + _rolls[roll + 2];
        }

        private int SpareBonus(int roll)
        {
            return _rolls[roll + 2];
        }

        private bool IsStrike(int roll)
        {
            return _rolls[roll] == 10;
        }

        private bool IsSpare(int roll)
        {
            return _rolls[roll] + _rolls[roll + 1] == 10;
        }

        public void Roll(int pins)
        {
            _rolls[_currentRoll++] = pins;
        }
    }
}
