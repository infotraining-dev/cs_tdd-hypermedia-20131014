﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Ninject.Modules;

namespace DependencyInjection
{
    class Program
    {
        static void PerformCalculation(Service service, int x, int y)
        {
            try
            {
                int result = service.Calculate(x, y);
                Console.WriteLine("Wynik obliczeń: {0}", result);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Błąd obliczeń.");
            }
        }

        public class IoCModule : NinjectModule
        {
            public override void Load()
            {
                Bind<ILogger>().To<FileLogger>().WithConstructorArgument("fileName", "log.dat");
                Bind<IFormatter>().To<ToUpperFormatter>().InSingletonScope();
                
            }
        }

        static void Main(string[] args)
        {
            var kernel = new StandardKernel(new IoCModule());

            Service service = kernel.Get<Service>();

            PerformCalculation(service, 20, 5);
            PerformCalculation(service, 4, 0);
        }
    }
}
