﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjection
{
    public class Service
    {
        private ILogger _logger;

        public Service(ILogger logger)
        {
            _logger = logger;
        }

        public int Calculate(int x, int y)
        {
            int result = 0;

            try
            {
                result = x/y;
            }
            catch (Exception ex)
            {
                _logger.LogMessage("Error: " + ex.Message);
                throw;
            }

            return result;
        }
    }
}
