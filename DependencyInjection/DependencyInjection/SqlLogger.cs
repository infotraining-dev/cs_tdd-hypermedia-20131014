﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DependencyInjection
{
    public interface ILogger
    {
        void LogMessage(string message);
    }

    public class SqlLogger : ILogger
    {
        public void LogMessage(string message)
        {
            Console.WriteLine("Logged in database: " + message);
        }
    }

    public interface IFormatter
    {
        string Format(string text);
    }

    public class ToUpperFormatter : IFormatter
    {
        public string Format(string text)
        {
            return text.ToUpper();
        }
    }

    public class FileLogger : ILogger
    {
        private string _fileName;
        private IFormatter _formatter;

        public FileLogger(string fileName, IFormatter formatter)
        {
            _fileName = fileName;
            _formatter = formatter;
        }

        public void LogMessage(string message)
        {
            Console.WriteLine("FileLog to file {0} - {1}", 
                              _fileName, _formatter.Format(message));
        }
    }
}   
