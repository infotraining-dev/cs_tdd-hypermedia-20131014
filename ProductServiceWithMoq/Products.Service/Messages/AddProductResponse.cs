namespace Products.Services.Messages
{
    public class AddProductResponse
    {
        public bool IsOk { get; set; }
        public string Message { get; set; }
    }
}