using Products.Domain;

namespace Products.Services.Messages
{
    public class AddProductRequest
    {
        public Product Product { get; set; }
    }
}