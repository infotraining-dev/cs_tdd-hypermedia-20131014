﻿using System.Collections.Generic;
using Products.Domain;

namespace Products.Services.Messages
{
    public class GetProductsResponse
    {
        public bool IsOk { get; set; }
        public IEnumerable<Product> Products { get; set; }
        public string Message { get; set; }
    }
}
