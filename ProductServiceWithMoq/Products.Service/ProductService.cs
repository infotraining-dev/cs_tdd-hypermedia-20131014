﻿/*
     *  1. Chcemy wyswietlic liste wszystkich produktów w kolejności alfabetycznej
     *  
     *  2. Chcemy dodać nowy produkt do bazy. 
     *      - Jesli istnieje produkt o takiej samej nazwie:
     *          + nie dodajemy
     *          + zwracamy komunikat
     *  
     *  3. Chcemy usunąć wszystkie wycofane produkty. Wycofane produkty zapisujemy do dziennika logow.  
     */

using System;
using System.Linq;
using Products.Domain.Repositories;
using Products.Services.Messages;

namespace Products.Services
{
    public class ProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public GetProductsResponse GetProducts()
        {
            GetProductsResponse result = new GetProductsResponse();

            try
            {
                var products = _productRepository.GetAll().OrderBy(p => p.Name);

                result.IsOk = true;
                result.Products = products;
            }
            catch (Exception)
            {
                result.IsOk = false;
                result.Message = "FATAL ERROR";
            }

            return result;
        }

        public AddProductResponse AddProduct(AddProductRequest addProductRequest)
        {
            if (_productRepository.FindByName(addProductRequest.Product.Name) == null)
            {
                _productRepository.Insert(addProductRequest.Product);

                return new AddProductResponse() {IsOk = true};
            }

            return new AddProductResponse() {IsOk = false, Message = "Cannot insert a product with duplicated name"};
        }
    }
}