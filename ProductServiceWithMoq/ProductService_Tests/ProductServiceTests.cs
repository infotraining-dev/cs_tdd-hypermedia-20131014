﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Products.Domain;
using Products.Domain.Repositories;
using Products.Services;
using Products.Services.Messages;

namespace ProductService_Tests
{
    public class WithService
    {
        protected Mock<IProductRepository> mqProductRepository;
        protected ProductService sut;

        [SetUp]
        public void SetupTest()
        {
            mqProductRepository = new Mock<IProductRepository>();
            sut = new ProductService(mqProductRepository.Object);
        }
    }

    [TestFixture]
    public class WhenGettingProducts : WithService
    {
        private List<Product> products;

        [SetUp]
        public void SetupTest()
        {
            products = new List<Product>()
            {
                new Product() {Id = 1, Name = "P1"},
                new Product() {Id = 3, Name = "P3"},
                new Product() {Id = 2, Name = "P2"},
                new Product() {Id = 4, Name = "P4"},
            };
        }

        [Test]
        public void ShouldReturnProductsInAlphabeticalOrder()
        {
            mqProductRepository.Setup(m => m.GetAll()).Returns(products);

            var expectedOrder = products.OrderBy(p => p.Name);

            var response = sut.GetProducts();

            CollectionAssert.AreEqual(expectedOrder, response.Products);
        }

        [Test]
        public void ShouldReturnAllProducts()
        {
            mqProductRepository.Setup(m => m.GetAll()).Returns(products);
            
            var result = sut.GetProducts();

            Assert.That(result.IsOk, Is.True);
            Assert.That(result.Products.Count(), Is.EqualTo(4));
        }

        [Test]
        public void ShouldHandleExceptionFromRepository()
        {
            mqProductRepository.Setup(m => m.GetAll()).Throws<Exception>();
            
            var result = sut.GetProducts();

            Assert.That(result.IsOk, Is.False);
            Assert.That(result.Message, Is.EqualTo("FATAL ERROR"));
        }
    }

    [TestFixture]
    public class WhenAddingProduct : WithService
    {
        Product product = new Product();

        [SetUp]
        public void SetupTest()
        {
            mqProductRepository = new Mock<IProductRepository>();
            sut = new ProductService(mqProductRepository.Object);
        }

        [Test]
        public void ShouldAddProductToRepository()
        {
            AddProductResponse response = sut.AddProduct(
                new AddProductRequest {Product = product});

            mqProductRepository.Verify(m => m.Insert(product), Times.Once);
            Assert.That(response.IsOk, Is.True);
        }

        [Test]
        public void ShouldNotInsertProductWithDuplicateName()
        {
            mqProductRepository.Setup(m => m.FindByName(product.Name)).Returns(product);

            var result = sut.AddProduct(new AddProductRequest() { Product = product });

            Assert.That(result.IsOk, Is.False);
            Assert.That(result.Message, Is.EqualTo("Cannot insert a product with duplicated name"));
        }
    }
}
