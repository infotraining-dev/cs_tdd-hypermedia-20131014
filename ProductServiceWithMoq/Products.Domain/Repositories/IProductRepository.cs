﻿using System.Collections.Generic;

namespace Products.Domain.Repositories
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAll();
        void Insert(Product product);
        Product FindByName(string name);
    }
}
